<?php

declare(strict_types=1);

use designerei\ContaoScrollAnimationBundle\EventListener\RegisterFieldsInPaletteListener;

$GLOBALS['TL_DCA']['tl_content']['config']['onload_callback'][] = [RegisterFieldsInPaletteListener::class, 'onLoadContentCallback'];

$GLOBALS['TL_DCA']['tl_content']['fields']['dataSal'] = [
    'exclude'                 => true,
    'inputType'               => 'select',
    'options'                 => array('fade','slide-up','slide-down','slide-left','slide-right','zoom-in','zoom-out','flip-out','flip-down','flip-left','flip-right'),
    'eval'                    => array('tl_class'=>'w50','includeBlankOption'=>true),
    'sql'                     => "varchar(32) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dataSalDuration'] = [
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('tl_class'=>'w50', 'maxlength'=>4),
    'sql'                     => "varchar(4) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dataSalDelay'] = [
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('tl_class'=>'w50', 'maxlength'=>4),
    'sql'                     => "varchar(4) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dataSalEasing'] = [
    'exclude'                 => true,
    'inputType'               => 'select',
    'default'                 => 'ease-in-out',
    'options'                 => array('linear', 'ease', 'ease-in', 'ease-out', 'ease-in-out', 'ease-in-cubic', 'ease-out-cubic', 'ease-in-out-cubic', 'ease-in-circ', 'ease-out-circ', 'ease-in-out-circ', 'ease-in-expo', 'ease-out-expo', 'ease-in-out-expo', 'ease-in-quad', 'ease-out-quad', 'ease-in-out-quad', 'ease-in-quart', 'ease-out-quart', 'ease-in-out-quart', 'ease-in-quint', 'ease-out-quint', 'ease-in-out-quint', 'ease-in-sine', 'ease-out-sine', 'ease-in-out-sine', 'ease-in-back', 'ease-out-back', 'ease-in-out-back'),
    'eval'                    => array('tl_class'=>'w50'),
    'sql'                     => "varchar(32) NOT NULL default 'ease-in-out'"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dataSalRepeat'] = [
    'exclude'                 => true,
    'inputType'               => 'checkbox',
    'eval'                    => ['tl_class'=>'w50 cbx'],
    'sql'                     => "char(1) NOT NULL default ''"
];
