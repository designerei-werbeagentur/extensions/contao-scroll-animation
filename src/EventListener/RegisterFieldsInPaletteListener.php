<?php

declare(strict_types=1);

namespace designerei\ContaoScrollAnimationBundle\EventListener;

use Contao\DataContainer;

final class RegisterFieldsInPaletteListener
{
   public function onLoadContentCallback(DataContainer $dataContainer): void
   {
       foreach ($GLOBALS['TL_DCA']['tl_content']['palettes'] as $k => $palette) {
           if (!\is_array($palette) && false !== strpos($palette, 'cssID')) {
               $GLOBALS['TL_DCA']['tl_content']['palettes'][$k] = str_replace(
                   '{expert_legend',
                   '{sal_legend:hide},dataSal,dataSalDuration,dataSalDelay,dataSalEasing,dataSalRepeat;{expert_legend',
                   $GLOBALS['TL_DCA']['tl_content']['palettes'][$k]
               );
           }
       }
   }
}
