<?php

declare(strict_types=1);

namespace designerei\ContaoScrollAnimationBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class ContaoScrollAnimationBundle extends Bundle
{
    public function getPath(): string
    {
        return \dirname(__DIR__);
    }
}
